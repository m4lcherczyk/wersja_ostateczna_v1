FROM python:latest
WORKDIR /opt
ADD . /opt
RUN pip install -r requirements.txt
CMD ["python", "wsgi.py"]




